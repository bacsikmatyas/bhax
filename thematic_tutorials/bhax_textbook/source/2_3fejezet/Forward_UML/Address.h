#ifndef ADDRESS_H
#define ADDRESS_H

class Address {

private:
	int street;
	int city;
	int state;
	int postalCode;
	int country;

public:
	void validate();
};

#endif
