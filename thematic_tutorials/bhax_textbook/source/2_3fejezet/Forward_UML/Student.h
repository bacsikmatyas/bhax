#ifndef STUDENT_H
#define STUDENT_H

class Student : Person {

private:
	int neptunID;
	int avgMark;

public:
	void takeCourse();
};

#endif
