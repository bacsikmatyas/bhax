import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MyTests {

    @Test
    public void multiplicationOfZeroIntegersShouldReturnZero() {
        LZWBinFa tester = new LZWBinFa(); // MyClass is tested

		for (char c : "01111001001001000111".toCharArray()) 
		{
			binfa.egyBitFeldolg(c);
		}
		assertEquals(4, binfa.getMelyseg(), 0.0);
		assertEquals(2.75, binfa.getAtlag(), 0.001);
		assertEquals(0.957427, binfa.getSzoras(), 0.0001);
    }
}