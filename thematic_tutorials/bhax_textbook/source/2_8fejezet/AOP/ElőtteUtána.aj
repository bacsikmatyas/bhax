public aspect ElőtteUtána {
    public pointcut fgvHivás() : call (public void HellóVilág.helló());

    before(): fgvHivás() {
        System.out.println("ElotteUtana> alma");
    }

    after(): fgvHivás() {
        System.out.println("ElotteUtana> körte");
    }
}