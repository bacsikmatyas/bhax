public class Liskovsertes{
    public static void main(String args[]){
            //Madar m = new Madar();
            Sas s1 = new Sas();
            Strucc s2 = new Strucc();
            
            System.out.println(s1.faj+": ");
            s1.repul();
            
            System.out.println(s2.faj+": ");
            s2.repul();
        }
}

class Madar{
    public void repul(){
        System.out.println("Repülök!");
    }
}

class Sas extends Madar{
    String faj = "Sas";
}

class Strucc extends Madar{
    String faj = "Strucc";
}
