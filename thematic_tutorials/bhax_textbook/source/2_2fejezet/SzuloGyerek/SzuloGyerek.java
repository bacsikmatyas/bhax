public class SzuloGyerek{
    public static void main(String args[]){
            Szulo szulo = new Gyerek();
            
            szulo.szuloTeszt();
            szulo.gyerekTeszt();
            
            
            
            
            /*
            Gyerek gyerek = new Gyerek();
            gyerek.gyerekTeszt();
            */
            
            /*
            Gyerek gyerek = new Szulo();
            gyerek.szuloTeszt();
            gyerek.gyerekTeszt();
            */
        }
        
}

class Szulo{
    public void szuloTeszt(){
        System.out.println("A szuloTeszt metódust elérem.");
    }
}

class Gyerek extends Szulo{
    public void gyerekTeszt(){
        System.out.println("A gyerekTeszt metódust nem érem el.");
    }
}
