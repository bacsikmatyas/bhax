#include <iostream>

using namespace std;

int main (){


	cout<<"Szóhossz függvénnyel:"<<endl;
	cout<<"\tInt: "<<sizeof(int)*8<<" bit"<<endl;

	cout<<"Szóhossz meghatározása bit eltolással: "<<endl;
	//BogoMIPS ciklus feje: while ((loops_per_sec <<= 1))

	int a = 1;
	int count = 1;
	while ((a <<= 1)){
		count++;
	}	

	cout<<"\tInt: "<<count<<" bit"<<endl;
	return 0;
}
