#include <stdio.h>

int main(){
        int a = 8;
        int b = 16;

        printf("%d\n",a); // --> 8
        printf("%d\n",b); // --> 16

	int tmp = a;
	a = b;
	b = tmp;        

        printf("%d\n",a); // --> 16
        printf("%d\n",b); // --> 8
        return 0;
}
