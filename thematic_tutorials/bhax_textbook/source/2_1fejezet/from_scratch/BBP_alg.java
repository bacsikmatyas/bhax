
import java.math.*;

public class BBP_alg{
    public static void main (String[] args){
        System.out.println(PiHex(1000000,10));
        
    }
    static String PiHex(int d, int n){
        String ret = "";
        
        double Pi16 = SjComp(d);
        int tmp = 0;
        
        char[] hex_chars = {'A','B','C','D','E','F'};
        
        for(int i = 0; i < n; i++){
            tmp = (int)Math.floor(Pi16*16);
            
            Pi16=(Pi16*16)-tmp;
            
            if(tmp<10){
                ret=ret+tmp;
            }
            else{
                ret=ret+hex_chars[tmp-10];
            }
            
        }
        
        return ret;
        
    }
    
    
    static double SjComp(int d){
        return (4*(Sj(d,1)) - 2*(Sj(d,4)) - (Sj(d,5)) - (Sj(d,6))) - Math.floor( 4*(Sj(d,1)) - 2*(Sj(d,4)) - (Sj(d,5)) - (Sj(d,6)));
    }
    
    static double Sj (int d, int j){
        double firstHalf = 0;
        double secondHalf = 0;
        
        for(int k = 0; k < d; k++){
        
            firstHalf = firstHalf+( (double)(mod16(d-k, 8*k+j))   / (double)( 8*k+j ) );
        
        }
        
        /*
        for(int k = d+1; k < 2*d; k++){
        
            secondHalf = secondHalf+(((double)(mod16(d-k, 8*k+j)))/(8*k+j));
        
        }
        */
        
        return (firstHalf+secondHalf)-Math.floor(firstHalf+secondHalf);
    }
    
    static long mod16 (int n, int k){
        int t = 1;
        while(t <= n){
            t = t*2;
        }
        
        long r = 1;
        
        while(true){
            if(n>=t){
                r = (16*r)%k;
                n = n-t;
            }
            
            t = t/2;
            
            if(t<1){
                break;
            }
            
            r = (r*r)%k;
            
        }
        
        return r;
        
    }
    
    
}
