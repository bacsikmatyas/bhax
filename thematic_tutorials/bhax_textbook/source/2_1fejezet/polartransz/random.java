
import java.math.*;


public class random{
    public static void main(String [] args){

        Rnd teszt = new Rnd();   
        
        for(int i = 0; i < 10; i++){
            System.out.println(teszt.get());
        }
    }
}
class Rnd{
    private boolean exist=false;
    private double value;
    
    
    public double get(){
        if (!exist){
        
            double u1, u2, v1, v2, w;
            do{
                u1 = (double)(Math.random());
                u2 = (double)(Math.random());
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
		    }
            while (w > 1);

            double r = Math.sqrt ((-2 * Math.log (w)) / w);

            this.value = r * v2; 
            this.exist = !exist;

            return r * v1; //idáig tart az algoritmus
        }
	
        else{
            exist = !exist;
            System.out.println("Nem kellett generálni.");
            return value;
        }
    }
    
}
