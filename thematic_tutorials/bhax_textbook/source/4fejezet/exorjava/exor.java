//import java.io.IOException;
//import java.io.RandomAccessFile;
//import java.util.ArrayList;
import java.util.Scanner;

public class exor{
    public static void main(String[] args){
        String jelszo = args[0];
        int jelszo_index = 0;
        
        Scanner sc = new Scanner(System.in);
        String sor;
        String tiszta_szoveg="";
        
        while(sc.hasNextLine()){
            sor = sc.nextLine();
            tiszta_szoveg+=sor+'\n';
        }
        System.out.println();
        System.out.print(tiszta_szoveg);
        
        String titkos_szoveg = "";
        for(int i = 0; i < tiszta_szoveg.length(); i++){
            titkos_szoveg += (char)(tiszta_szoveg.charAt(i) ^ jelszo.charAt(jelszo_index));
            jelszo_index = (jelszo_index+1) % jelszo.length();
        }
        System.out.println();
        System.out.print(titkos_szoveg);
        
        
        //Oldás ellenőrzésként
        jelszo_index = 0;
        String oldott_szoveg = "";
        for(int i = 0; i < titkos_szoveg.length(); i++){
            oldott_szoveg += (char)(titkos_szoveg.charAt(i) ^ jelszo.charAt(jelszo_index));
            jelszo_index = (jelszo_index+1) % jelszo.length();
        }
        System.out.println();
        System.out.print(oldott_szoveg);
    }
}
