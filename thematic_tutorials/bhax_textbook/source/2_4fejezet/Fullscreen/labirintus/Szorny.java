/*
 * Szorny.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintus szornyeit megado osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 * @see javattanitok.labirintus.Labirintus
 */
public class Szorny extends Szereplo {
    /** A megevett hosok szama. */
    int megevettHosokSzama;
    /** A megevett kincsek szama. */
    int megevettKincsekSzama;
    /**
     * Letrehoz egy <code>Szorny</code> objektumot.
     *
     * @param      labirintus       amibe a szornyet helyezzuk.
     */
    public Szorny(Labirintus labirintus) {
        super(labirintus);
    }
    /**
     * A szornyek mozgasanak vezerlese, ami szerint szornyek
     * a hos feles igyekeznek.
     *
     * @param hos aki fele a szorny igyekszik
     */
    public void lep(Hos hos) {
        
        int tavolsag = tavolsag(hos);
        // Abba az iranyba levo poziciora lep, amelyben kozelebb kerul a hos.
        if(!labirintus.fal(oszlop, sor-1))
            if(tavolsag(oszlop, sor-1, hos) < tavolsag) {
            lepFol();
            return;
            
            }
        
        if(!labirintus.fal(oszlop, sor+1))
            if(tavolsag(oszlop, sor+1, hos) < tavolsag) {
            lepLe();
            return;
            
            }
        
        if(!labirintus.fal(oszlop-1, sor))
            if(tavolsag(oszlop-1, sor, hos) < tavolsag) {
            lepBalra();
            return;
            
            }
        
        if(!labirintus.fal(oszlop+1, sor))
            if(tavolsag(oszlop+1, sor, hos) < tavolsag) {
            lepJobbra();
            return;
            
            }
        
    }
    /**
     * A szorny megette a host?
     *
     * @param      hos       aki bolyong a labirintusban.
     */
    public boolean megesz(Hos hos) {
        
        if(oszlop == hos.oszlop()
        && sor == hos.sor()) {
            
            ++megevettHosokSzama;
            return true;
            
        } else
            return false;
    }    
    /**
     * Szamolgatja a megevett kincseket.
     *
     * @param      kincs       amit eppen megettem.
     */
    public void megtalaltam(Kincs kincs) {
        
        ++megevettKincsekSzama;
        
    }
        /**
     * A {@code Szorny} objektum sztring reprezentaciojat adja
     * meg.
     *
     * @return String az objektum sztring reprezentacioja.
     */
    public String toString() {
        
        return "SZORNY megevett hosok = "
                + megevettHosokSzama
                + "megevett kincsek = "
                + megevettKincsekSzama;
    }    
}
