/*
 * Labirintus.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintust leir� osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class Labirintus {
    /** A labirintus szelessege. */
    protected int szelesseg;
    /** A labirintus magassaga. */
    protected int magassag;
    /** A labirintus szerkezete: hol van fal, hol jarat. */
    protected boolean[][] szerkezet;
    /** A falat a true ertek jelenti. */
    final static boolean FAL = true;
    /** Milyen allapotban van eppen a jatek. */
    protected int jatekAllapot = 0;
    /** Normal muk�des, a h�ssel id�k�zben semmi nem t�rtent. */
    public static final int JATEK_MEGY_HOS_RENDBEN = 0;    
    /** A h�st eppen megettek, de meg van elete. */
    public final static int JATEK_MEGY_MEGHALT_HOS = 1;
    /** Vege a jateknak, a jatekos gy�z�tt. */
    public final static int JATEK_VEGE_MINDEN_KINCS_MEGVAN = 2;
    /** Vege a jateknak, a jatekos vesztett. */
    public final static int JATEK_VEGE_MEGHALT_HOS = 3;
    /** A labirintus kincsei. */
    protected Kincs [] kincsek;
    /** A labirintus sz�rnyei. */
    protected Sz�rny [] sz�rnyek;
    /**
     * Letrehoz egy megadott szerkezetu
     * {@code Labirintus} objektumot.
     */
    public Labirintus() {
        szerkezet = new boolean[][]{
            
    {false, false,  false, true,  false, true,  false, true,  true,  true },
    {false, false, false, false, false, false, false, false, false, false},
    {true,  false, true,  false, true,  false, true,  false, true,  false},
    {false, false, false, false, true,  false, true,  false, false, false},
    {false, true,  true,  false, false, false, true,  true,  false, true },
    {false, false, false, false, true,  false, false, false, false, false},
    {false,  true,  false, false,  false, true,  false, true,  true,  false},
    {false,  false, false, true,  false, true,  false, true,  false, false},
    {false, true, false, false, false, false, false, false, false, true },
    {false, false, false, false,  true,  false, false, false,  true,  true }
    
        };
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
    }
    /**
     * Letrehoz egy parameterben kapott szerkezetu <code>Labirintus</code> 
     * objektumot.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      sz�rnyekSzama      a sz�rnyek szama a labirintusban.
     * @exception  RosszLabirintusKivetel  ha a labirintust definial� t�mb 
     * nincs elkeszitve.
     */
    public Labirintus(boolean[][] szerkezet, int kincsekSzama, int sz�rnyekSzama)
    throws RosszLabirintusKivetel {
        
        if(szerkezet == null)
            throw new RosszLabirintusKivetel("A labirintust definial� t�mb nincs elkeszitve.");
        
        this.szerkezet = szerkezet;
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
        kincsekSz�rnyek(kincsekSzama, sz�rnyekSzama);
        
    }
    /**
     * Letrehoz egy megadott meretu, veletlen szerkezetu
     * <code>Labirintus</code> objektumot.
     *
     * @param      szelesseg          a labirintus szelessege.
     * @param      magassag           a labirintus magassaga.
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      sz�rnyekSzama      a sz�rnyek szama a labirintusban.
     */
    public Labirintus(int szelesseg, int magassag,
            int kincsekSzama, int sz�rnyekSzama) {
        
        this.magassag = magassag;
        this.szelesseg = szelesseg;
        
        szerkezet = new boolean[magassag][szelesseg];
        java.util.Random veletlenGenerator = new java.util.Random();
        
        for(int i=0; i<magassag; ++i)
            for(int j=0; j<szelesseg; ++j)
                if(veletlenGenerator.nextInt()%3 == 0)
                    // a labirintus egy harmada lesz fal
                    szerkezet[magassag][szelesseg] = false;
                else
                    // ket harmada pedig jarat
                    szerkezet[magassag][szelesseg] = true;
        
        kincsekSz�rnyek(kincsekSzama, sz�rnyekSzama);
        
    }
    /**
     * Letrehoz egy 10x10-es, beepitett szerkezetu <code>Labirintus</code>
     * objektumot.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      sz�rnyekSzama      a sz�rnyek szama a labirintusban.
     */
    public Labirintus(int kincsekSzama, int sz�rnyekSzama) {
        
        this();
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
        kincsekSz�rnyek(kincsekSzama, sz�rnyekSzama);
        
    }
    /**
     * Egy megfelel� szerkezetu sz�veges allomanyb�l elkeszit egy uj a 
     * <code>Labirintus</code> objektumot.
     * A sz�veges allomany szerkezete a k�vetkez�:
     * <pre>
     * // A labirintus szerkezetet megad� allomany, szerkezete a k�vetkez�:
     * // a kincsek szama
     * // a sz�rnyek szama
     * // a labirintus szelessege
     * // magassaga
     * // fal=1 jarat=0 ...
     * // .
     * // .
     * // .
     * 6
     * 3
     * 10
     * 10
     * 0 0 0 1 0 1 0 1 1 1
     * 0 0 0 0 0 0 0 0 0 0
     * 1 0 1 0 1 0 1 0 1 0
     * 0 0 0 0 1 0 1 0 0 0
     * 0 1 1 0 0 0 1 1 0 1
     * 0 0 0 0 1 0 0 0 0 0
     * 0 1 0 0 0 1 0 1 1 0
     * 0 0 0 1 0 1 0 1 0 0
     * 0 1 0 0 0 0 0 0 0 1
     * 0 0 0 0 1 0 0 0 1 1
     * </pre>
     *
     * @param      labirintusFajlNev       a labirintust definial�, megfelel� 
     * szerkezetu sz�veges allomany neve.
     * @exception  RosszLabirintusKivetel  ha a labirintust definial� allomany 
     * nincs meg, nem a megfelel� szerkezetu, vagy gond van az olvasasaval.
     */
    public Labirintus(String labirintusFajlNev) throws RosszLabirintusKivetel {
        
        int kincsekSzama = 6;  // ezeknek a kezd�ertekeknek nincs jelent�sege,
        int sz�rnyekSzama = 3; // mert majd a fajlb�l olvassuk be, amiben ha a 
        // negy f� adat hibas, akkor nem is epitjuk fel a labirintust.
        
        // Csatorna a sz�veges allomany olvasasahoz
        java.io.BufferedReader sz�vegesCsatorna = null;
        
        try {
            sz�vegesCsatorna = new java.io.BufferedReader(
                    new java.io.FileReader(labirintusFajlNev));
            
            String sor = sz�vegesCsatorna.readLine();
            
            while(sor.startsWith("//"))
                sor = sz�vegesCsatorna.readLine();
            
            try {
                
                kincsekSzama = Integer.parseInt(sor);
                
                sor = sz�vegesCsatorna.readLine();
                sz�rnyekSzama = Integer.parseInt(sor);
                
                sor = sz�vegesCsatorna.readLine();
                szelesseg = Integer.parseInt(sor);
                
                sor = sz�vegesCsatorna.readLine();
                magassag = Integer.parseInt(sor);
                
                szerkezet = new boolean[magassag][szelesseg];
                
            } catch(java.lang.NumberFormatException e) {
                
                throw new RosszLabirintusKivetel("Hibas a kincsek, sz�rnyek szama, szelesseg, magassag megadasi resz.");
                
            }
            
            for(int i=0; i<magassag; ++i) {
                
                sor = sz�vegesCsatorna.readLine();
                
                java.util.StringTokenizer st =
                        new java.util.StringTokenizer(sor);
                
                for(int j=0; j<szelesseg; ++j) {
                    String tegla = st.nextToken();
                    
                    try {
                        
                        if(Integer.parseInt(tegla) == 0)
                            szerkezet[i][j] = false;
                        else
                            szerkezet[i][j] = true;
                        
                    } catch(java.lang.NumberFormatException e) {
                        
                        System.out.println(i+". sor "+j+". oszlop "+e);
                        szerkezet[i][j] = false;
                        
                    }
                }
            }
            
        } catch(java.io.FileNotFoundException e1) {
            
            throw new RosszLabirintusKivetel("Nincs meg a fajl: " + e1);
            
        } catch(java.io.IOException e2) {
            
            throw new RosszLabirintusKivetel("IO kivetel t�rtent: "+e2);
            
        } catch(java.util.NoSuchElementException e3) {
            
            throw new RosszLabirintusKivetel("Nem j� a labirintus szerkezete: "
                    +e3);
            
        } finally {
            
            if(sz�vegesCsatorna != null) {
                
                try{
                    sz�vegesCsatorna.close();
                } catch(Exception e) {}
                
            }
            
        }
        
        // Ha ide eljutottunk, akkor felepult a labirintus,
        // lehet benepesiteni:
        kincsekSz�rnyek(kincsekSzama, sz�rnyekSzama);
        
    }
    /**
     * Letrehozza a kincseket es a sz�rnyeket.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      sz�rnyekSzama      a sz�rnyek szama a labirintusban.
     */
    private void kincsekSz�rnyek(int kincsekSzama, int sz�rnyekSzama) {
        // Kincsek letrehozasa
        kincsek = new Kincs[kincsekSzama];
        for(int i=0; i<kincsek.length; ++i)
            kincsek[i] = new Kincs(this, (i+1)*100);
        // Sz�rnyek letrehozasa
        sz�rnyek = new Sz�rny[sz�rnyekSzama];
        for(int i=0; i<sz�rnyek.length; ++i)
            sz�rnyek[i] = new Sz�rny(this);
        
    }
    /**
     * Megadja a jatek aktualis allapotat.
     *
     * @return int a jatek aktualis allapota.
     */
    public int allapot() {
        
        return jatekAllapot;
        
    }
    /**
     * A labirintus mikrovilag eletenek egy pillanata: megnezi, hogy a bolyong�
     * h�s ratalalt-e a kincsekre, vagy a sz�rnyek a h�sre. Ennek megfelel�en
     * megvaltozik a jatek allapota.
     *
     * @param h�s aki a labirintusban bolyong.
     * @return int a jatek allapotat leir� k�d.
     */
    public int bolyong(H�s h�s) {
        
        boolean mindMegvan = true;
        
        for(int i=0; i < kincsek.length; ++i) {
            
            // A h�s ratalalt valamelyik kincsre?
            if(kincsek[i].megtalalt(h�s))
                h�s.megtalaltam(kincsek[i]);
            
            // ha ez egyszer is teljesul, akkor nincs minden kincs megtalalva
            if(!kincsek[i].megtalalva())
                mindMegvan = false;
            
        }
        
        if(mindMegvan) {
            
            jatekAllapot = JATEK_VEGE_MINDEN_KINCS_MEGVAN;
            return jatekAllapot;
            
        }
        
        for(int i=0; i < sz�rnyek.length; ++i) {
            
            sz�rnyek[i].lep(h�s);
            
            if(sz�rnyek[i].megesz(h�s))  {
                jatekAllapot = JATEK_MEGY_MEGHALT_HOS;
                
                if(h�s.megettek())
                    jatekAllapot = JATEK_VEGE_MEGHALT_HOS;
                
                return jatekAllapot;
            }
            
        }
        
        return JATEK_MEGY_HOS_RENDBEN;
    }
    /**
     * Madadja, hogy fal-e a labirintus adott oszlop, sor pozici�ja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha a pozici� fal vagy nincs a labirintusban.
     */
    public boolean fal(int oszlop, int sor) {
        
        if(!(oszlop >= 0 && oszlop <= szelesseg-1
                && sor >= 0 && sor <= magassag-1))
            return FAL;
        else
            return szerkezet[sor][oszlop] == FAL;
        
    }
    /**
     * Madadja a labirintus szelesseget.
     *
     * @return int a labirintus szelessege.
     */
    public int szelesseg() {
        
        return szelesseg;
        
    }
    /**
     * Madadja a labirintus magassagat.
     *
     * @return int a labirintus magassaga.
     */
    public int magassag() {
        
        return magassag;
        
    }
    /**
     * Megadja a labirintus szerkezetet.
     *
     * @return boolean[][] a labirintus szerkezete.
     */
    public boolean[][] szerkezet() {
        
        return szerkezet;
        
    }
    /**
     * Megadja a labirintus kincseit.
     *
     * @return Kincs[] a labirintus kincsei.
     */
    public Kincs[] kincsek() {
        
        return kincsek;
        
    }
    /**
     * Megadja a labirintus sz�rnyeit.
     *
     * @return Sz�rny[] a labirintus sz�rnyei.
     */
    public Sz�rny[] sz�rnyek() {
        
        return sz�rnyek;
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet a System.out-ra.
     */
    public void nyomtat() {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                if(szerkezet[i][j])
                    System.out.print("|FAL");
                else
                    System.out.print("|   ");
                
            }
            
            System.out.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szerepl�it a System.out-ra.
     *
     * @param h�s akit szinten belenyomtat a labirintusba.
     */
    public void nyomtat(H�s h�s) {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSz�rny = vanSz�rny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanH�s = (i == h�s.sor() && j == h�s.oszlop());
                
                if(szerkezet[i][j])
                    System.out.print("|FAL");
                else if(vanSz�rny && vanKincs && vanH�s)
                    System.out.print("|SKH");
                else if(vanSz�rny && vanKincs)
                    System.out.print("|SK ");
                else if(vanKincs && vanH�s)
                    System.out.print("|KH ");
                else if(vanSz�rny && vanH�s)
                    System.out.print("|SH ");
                else if(vanKincs)
                    System.out.print("|K  ");
                else if(vanH�s)
                    System.out.print("|H  ");
                else if(vanSz�rny)
                    System.out.print("|S  ");
                else
                    System.out.print("|   ");
                
            }
            
            System.out.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szerepl�it egy
     * karakteres csatornaba.
     *
     * @param h�s akit szinten belenyomtat a labirintusba.
     * @param csatorna ahova nyomtatunk.
     */
    public void nyomtat(H�s h�s, java.io.PrintWriter csatorna) {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSz�rny = vanSz�rny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanH�s = (i == h�s.sor() && j == h�s.oszlop());
                
                if(szerkezet[i][j])
                    csatorna.print("|FAL");
                else if(vanSz�rny && vanKincs && vanH�s)
                    csatorna.print("|SKH");
                else if(vanSz�rny && vanKincs)
                    csatorna.print("|SK ");
                else if(vanKincs && vanH�s)
                    csatorna.print("|KH ");
                else if(vanSz�rny && vanH�s)
                    csatorna.print("|SH ");
                else if(vanKincs)
                    csatorna.print("|K  ");
                else if(vanH�s)
                    csatorna.print("|H  ");
                else if(vanSz�rny)
                    csatorna.print("|S  ");
                else
                    csatorna.print("|   ");
                
            }
            
            csatorna.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szerepl�it egy sztringbe.
     *
     * @param h�s akit szinten belenyomtat a labirintusba.
     * @return String a kinyomtatott labirintus
     */
    public String kinyomtat(H�s h�s) {
        
        StringBuffer stringBuffer = new StringBuffer();
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSz�rny = vanSz�rny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanH�s = (i == h�s.sor() && j == h�s.oszlop());
                
                if(szerkezet[i][j])
                    stringBuffer.append("|FAL");
                else if(vanSz�rny && vanKincs && vanH�s)
                    stringBuffer.append("|SKH");
                else if(vanSz�rny && vanKincs)
                    stringBuffer.append("|SK ");
                else if(vanKincs && vanH�s)
                    stringBuffer.append("|KH ");
                else if(vanSz�rny && vanH�s)
                    stringBuffer.append("|SH ");
                else if(vanKincs)
                    stringBuffer.append("|K  ");
                else if(vanH�s)
                    stringBuffer.append("|H  ");
                else if(vanSz�rny)
                    stringBuffer.append("|S  ");
                else
                    stringBuffer.append("|   ");
                
            }
            
            stringBuffer.append("\n");
            
        }
        
        return stringBuffer.toString();
    }
    /**
     * Madadja, hogy van-e megtalalhat� kincs a labirintus
     * adott oszlop, sor pozici�ja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha van.
     */
    boolean vanKincs(int sor, int oszlop) {
        
        boolean van = false;
        
        for(int i=0; i<kincsek.length; ++i)
            if(sor == kincsek[i].sor()
            && oszlop == kincsek[i].oszlop()
            && !kincsek[i].megtalalva()) {
            van = true;
            break;
            }
        
        return van;
    }
    /**
     * Madadja, hogy van-e sz�rny a labirintus adott oszlop,
     * sor pozici�ja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha van.
     */
    boolean vanSz�rny(int sor, int oszlop) {
        
        boolean van = false;
        
        for(int i=0; i<sz�rnyek.length; ++i)
            if(sor == sz�rnyek[i].sor()
            && oszlop == sz�rnyek[i].oszlop()) {
            van = true;
            break;
            }
        
        return van;
    }
    /**
     * A labirintussal kapcsolatos apr�sagok �nall� kipr�balasara
     * szolgal ez az indit� met�dus.
     *
     * @param args parancssor-argumentumok nincsenek.
     */
    public static void main(String[] args) {
        
        Labirintus labirintus = new Labirintus(6, 3);
        H�s h�s = new H�s(labirintus);
        
        System.out.println(labirintus.getClass());
        System.out.println(h�s.getClass());
        
    }
}
