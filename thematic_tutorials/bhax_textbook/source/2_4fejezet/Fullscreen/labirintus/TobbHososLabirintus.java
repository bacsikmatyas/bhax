/*
 * TobbHososLabirintus.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A tobb hosos labirintust leiro osztaly, ahol egy hos halala
 * mar nem jelenti a labirintus jatek veget. A jatek allapotat
 * a korabbi jatekokban a labirintushoz kapcsoltuk, most, hogy
 * olyan tovabbfejlesztett labirintust akarunk, amiben tobb hos
 * is bolyonghat, ugy erezzuk, hogy a jatek vege inkabb a hoshoz
 * tartozik, semmint a labirintushoz. Mindketto igaz: mert, ha a
 * kincsek fogynak el, akkor a labirintus oldalarol van vege a
 * jateknak.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class TobbHososLabirintus extends Labirintus {
    /**
     * Argumentum nelkuli konstruktor, gyerekek implicit super()-ehez.
     */
    public TobbHososLabirintus() {}
    /**
     * A <code>TobbHososLabirintus</code> objektum elkeszitese.
     *
     * @param      labirintusFajlNev       a labirintust definialo, megfelelo 
     * szerkezetu szoveges allomany neve.
     * @exception  RosszLabirintusKivetel  ha a labirintust definialo allomany nem 
     * a megfelelo szerkezetu
     */
    public TobbHososLabirintus(String labirintusFajlNev) throws 
            RosszLabirintusKivetel {
        
        super(labirintusFajlNev);
        
    }
    /**
     * Az os megfelelo metodusanak elfedese, mert ez a JATEK_VEGE_MEGHALT_HOS
     * csak a hos veget jelenti, a labirintuset nem!
     *
     * @see Labirintus#bolyong(Hos hos)
     * @param hos aki a labirintusban bolyong.
     * @return int a jatek allapotat leiro kod.
     */
    public int bolyong(Hos hos) {
        
        boolean mindMegvan = true;
        
        for(int i=0; i < kincsek.length; ++i) {
            
            // A hos ratalalt valamelyik kincsre?
            if(kincsek[i].megtalalt(hos))
                hos.megtalaltam(kincsek[i]);
            
            // ha ez egyszer is teljesul, akkor nincs minden kincs megtalalva
            if(!kincsek[i].megtalalva())
                mindMegvan = false;
            
        }
        
        if(mindMegvan) {
            
            jatekAllapot = JATEK_VEGE_MINDEN_KINCS_MEGVAN;
            return jatekAllapot;
            
        }
        
        for(int i=0; i < szornyek.length; ++i) {
            
            szornyek[i].lep(hos);
            
            if(szornyek[i].megesz(hos))  {
                
                if(hos.megettek())
                    // De ez a jatek vege csak a hos veget
                    // jelenti, a labirintuset nem!
                    return JATEK_VEGE_MEGHALT_HOS;
                else
                    return JATEK_MEGY_MEGHALT_HOS;
                
            }            
        }
        
        return JATEK_MEGY_HOS_RENDBEN;
    }
}               
