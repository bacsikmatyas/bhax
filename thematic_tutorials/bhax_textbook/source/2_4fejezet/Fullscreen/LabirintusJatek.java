/*
 * LabirintusJatek.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok;

import javattanitok.labirintus.*;
/**
 * A labirintus csomag absztrahalta labirintus mikrovilaganak egy
 * "teljes kepernyos" (Full Screen Exclusive Mode API-s) eletre
 * keltesere ad peldat ez az osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 * @see javattanitok.LabirintusVilag
 * @see javattanitok.LabirintusApplet
 * @see javattanitok.LabirintusServlet
 * @see javattanitok.LabirintusMIDlet
 * @see javattanitok.HalozatiLabirintus
 * @see javattanitok.TavoliLabirintus
 * @see javattanitok.KorbasLabirintus
 * @see javattanitok.ElosztottLabirintus
 */
public class LabirintusJatek extends java.awt.Frame
        implements Runnable {
    /** A labirintus. */
    Labirintus labirintus;
    /** A hos. */
    Hos hos;
    /** A jatekbeli ido meresere.*/
    private long ido = 0;
    /** Jelzi a jatek veget, ezutan a jatek alapota mar nem valtozik. */
    private boolean jatekVege = false;
    /** A jatek vegen a jatekost tajekoztato uzenet. */
    String vegeUzenet = "Vege a jateknak!";
    /** Jelzi, hogy a program terminalhat. */
    private boolean jatekKilep = false;
    /** A labirintus szereploihez rendelt kepek. Ebben a peldaban mar
     * BufferedImage kepeket hasznalunk, mert majd a teljesitmeny javitas
     * apropojan ezeket a grafikus konfiguracionkhoz igazitjuk. */
    java.awt.image.BufferedImage falKep;
    java.awt.image.BufferedImage jaratKep;
    java.awt.image.BufferedImage hosKep;
    java.awt.image.BufferedImage szornyKep;
    java.awt.image.BufferedImage kincsKep;
    // A fullscreenbe kapcsolashoz
    java.awt.GraphicsDevice graphicsDevice;
    // A megjeleniteshez
    java.awt.image.BufferStrategy bufferStrategy;
    /**
     * A <code>LabirintusJatek</code> objektum elkeszitese.
     *
     * @param      labirintusFajlNev       a labirintust definialo, megfelelo
     * szerkezetu szoveges allomany neve.
     * @exception  RosszLabirintusKivetel  ha a labirintust definialo allomany nem 
     * a megfelelo szerkezetu
     */
    public LabirintusJatek(String labirintusFajlNev) 
    throws RosszLabirintusKivetel {
        /* A labirintus felepitese. */
        // A labirintus elkeszitese allomanybol
        labirintus = new Labirintus(labirintusFajlNev);
        // A hos elkeszitese es a kezdo poziciojanak beallitasa
        hos = new Hos(labirintus);
        // A hos kezdo pozicioja
        hos.sor(9);
        hos.oszlop(0);
        /* Teljes kepernyos modba probalunk valtani. */
        // A lokalis grafikus kornyezet elkerese
        java.awt.GraphicsEnvironment graphicsEnvironment
                = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment();
        // A grafikus kornyzetbol a kepernyovel dolgozunk
        graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        // Probalunk teljes kepernyos, most specialisan 1024x768-ba valtani
        teljesKepernyosMod(graphicsDevice);
        // Atadjuk a grafikus konfiguraciot a kompatibilis kepek elkeszitesehez
        kepEroforrasokBetoltese(graphicsDevice.getDefaultConfiguration());
        // A hos mozgatasa a KURZOR billenytukkel, ESC kilep
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent billentyuEsemeny) {
                
                int billentyu = billentyuEsemeny.getKeyCode();
                
                if(!jatekVege)
                    switch(billentyu) { // hos mozgatasa
                        
                        case java.awt.event.KeyEvent.VK_UP:
                            hos.lepFol();
                            break;
                        case java.awt.event.KeyEvent.VK_DOWN:
                            hos.lepLe();
                            break;
                        case java.awt.event.KeyEvent.VK_RIGHT:
                            hos.lepJobbra();
                            break;
                        case java.awt.event.KeyEvent.VK_LEFT:
                            hos.lepBalra();
                            break;
                            
                    }
                    // Kilepes a jatekbol
                    if(billentyu == java.awt.event.KeyEvent.VK_ESCAPE)
                        jatekKilep = true;
                    
                    // A jatekban tortent valtozasok a kepernyon 
                    // is jelenjenek meg
                    rajzolniKell();
                    
            };
        });
        
        // A jatekbeli ido folyasat biztosito szal elkeszitese es inditasa
        new Thread(this).start();
    }
    /** A jatek idobeli fejlodesenek vezerlese. */
    synchronized public void run() {
        
        while(!jatekKilep) {
            
            // Aktiv rendereles
            rajzol();
            
            idoegyseg();
            
            switch(labirintus.bolyong(hos)) {
                
                case Labirintus.JATEK_MEGY_HOS_RENDBEN:
                    break;
                case Labirintus.JATEK_MEGY_MEGHALT_HOS:
                    // Meg van elete, visszatesszuk a kezdo poziciora
                    hos.sor(9);
                    hos.oszlop(0);
                    break;
                case Labirintus.JATEK_VEGE_MINDEN_KINCS_MEGVAN:
                    vegeUzenet = "Gyoztel, vege a jateknak!";
                    jatekVege = true;
                    break;
                case Labirintus.JATEK_VEGE_MEGHALT_HOS:
                    vegeUzenet = "Vesztettel, vege a jateknak!";
                    jatekVege = true;
                    break;
                    
            }
            
        }
        // Kilepes a jatekbol
        setVisible(false);
        graphicsDevice.setFullScreenWindow(null);
        System.exit(0);
        
    }
    /**
     * Ebreszto az varakozo rajzolast vegzo szalnak, ki kell rajzolni a jatek 
     * grafikus feluletet.
     */
    synchronized public void rajzolniKell() {
        
        notify();
        
    }
    /**
     * Megadja, hogy milyen gyorsan telik az ido a jatekban.
     */
    private void idoegyseg() {
        
        ++ ido;
        
        try {
            
            wait(1000);
            
        } catch (InterruptedException e) {}
        
    }
    /**
     * Kep eroforrasok betoltese.
     *
     * @param   graphicsConfiguration   a grafikus komfiguracioval kompatibilis
     * kepek keszitesehez.
     */
    public void kepEroforrasokBetoltese(java.awt.GraphicsConfiguration 
            graphicsConfiguration) {
        
        falKep = kompatibilisKep("fal.png", graphicsConfiguration);
        jaratKep = kompatibilisKep("jarat.png", graphicsConfiguration);
        hosKep = kompatibilisKep("hos.png", graphicsConfiguration);
        szornyKep = kompatibilisKep("szorny.png", graphicsConfiguration);
        kincsKep = kompatibilisKep("kincs.png", graphicsConfiguration);
        
    }
    /**
     * A grafikus konfiguraciohoz igazitot kep.
     *
     * @param   kepNev   a kep allomany neve
     * @param   graphicsConfiguration   a grafikus komfiguracioval kompatibilis
     * kepek keszitesehez.
     */
    public java.awt.image.BufferedImage kompatibilisKep(String kepNev,
            java.awt.GraphicsConfiguration graphicsConfiguration) {
        // Kepet legegyszeruben a Swing-beli ImageIcon-al tolthetunk be:
        java.awt.Image kep = new javax.swing.ImageIcon
                (kepNev).getImage();
        // ebbol BufferedImage-et keszitunk, hogy hozzaferjunk a transzparencia
        // ertekhez (pl. a hos, a kincs es a szorny transzparens nalunk)
        java.awt.image.BufferedImage bufferedImage =
                new java.awt.image.BufferedImage(kep.getWidth(null), 
                kep.getHeight(null),
                java.awt.image.BufferedImage.TYPE_INT_ARGB);
        
        java.awt.Graphics2D g0 = bufferedImage.createGraphics();
        g0.drawImage(kep, 0, 0, null);
        g0.dispose();
        // Az elozo lepeshez hasonlo lepesben most egy olyan BufferedImage-et,
        // keszitunk, ami kompatibilis a grafikus konfiguracionkkal
        java.awt.image.BufferedImage kompatibilisKep
                = graphicsConfiguration.createCompatibleImage(
                bufferedImage.getWidth(), bufferedImage.getHeight(),
                bufferedImage.getColorModel().getTransparency());
        
        java.awt.Graphics2D g = kompatibilisKep.createGraphics();
        g.drawImage(bufferedImage, 0, 0, null);
        g.dispose();
        
        return kompatibilisKep;
    }
    /**
     * A jatek grafikus feluletenek aktiv renderelese.
     */
    public void rajzol() {
        
        java.awt.Graphics g = bufferStrategy.getDrawGraphics();
        
        // A labirintus kirajzolasa
        for(int i=0; i<labirintus.szelesseg(); ++i) {
            for(int j=0; j<labirintus.magassag(); ++j) {
                
                if(labirintus.szerkezet()[j][i])
                    g.drawImage(falKep, i*falKep.getWidth(),
                            j*falKep.getHeight(), null);
                else
                    g.drawImage(jaratKep, i*jaratKep.getWidth(),
                            j*jaratKep.getHeight(), null);
                
            }
        }
        
        // A kincsek kirajzolasa
        Kincs[] kincsek = labirintus.kincsek();
        for(int i=0; i<kincsek.length; ++i) {
            g.drawImage(kincsKep,
                    kincsek[i].oszlop()*kincsKep.getWidth(),
                    kincsek[i].sor()*kincsKep.getHeight(), null);
            // Ha mar megvan a kics, akkor athuzzuk
            if(kincsek[i].megtalalva()) {
                g.setColor(java.awt.Color.red);
                g.drawLine(kincsek[i].oszlop()*kincsKep.getWidth(),
                        kincsek[i].sor()*kincsKep.getHeight(),
                        kincsek[i].oszlop()*kincsKep.getWidth()
                        + kincsKep.getWidth(),
                        kincsek[i].sor()*kincsKep.getHeight()
                        + kincsKep.getHeight());
                g.drawLine(kincsek[i].oszlop()*kincsKep.getWidth()
                +kincsKep.getWidth(),
                        kincsek[i].sor()*kincsKep.getHeight(),
                        kincsek[i].oszlop()*kincsKep.getWidth(),
                        kincsek[i].sor()*kincsKep.getHeight()
                        + kincsKep.getHeight());
            } else {
                // ellenkezo esetben kiirjuk az erteket
                g.setColor(java.awt.Color.yellow);
                g.drawString(""+kincsek[i].ertek(),
                        kincsek[i].oszlop()*kincsKep.getWidth()
                        + kincsKep.getWidth()/2,
                        kincsek[i].sor()*kincsKep.getHeight()
                        + kincsKep.getHeight()/2);
            }
        }
        
        // A szornyek kirajzolasa
        Szorny[] szornyek = labirintus.szornyek();
        for(int i=0; i<szornyek.length; ++i)
            g.drawImage(szornyKep,
                    szornyek[i].oszlop()*szornyKep.getWidth(),
                    szornyek[i].sor()*szornyKep.getHeight(), null);
        
        // A hos kirajzolasa
        g.drawImage(hosKep,
                hos.oszlop()*hosKep.getWidth(),
                hos.sor()*hosKep.getHeight(), null);
        
        // A jatek aktualis adataibol nehany kiiratasa
        g.setColor(java.awt.Color.black);
        
        g.drawString("Eletek szama: "+hos.eletek(), 10, 40);
        g.drawString("Gyujtott ertek: "+hos.pontszam(), 10, 60);
        g.drawString("Ido: "+ido, 10, 80);
        
        if(jatekVege)
            g.drawString(vegeUzenet, 420, 350);
        
        g.dispose();
        if (!bufferStrategy.contentsLost())
            bufferStrategy.show();
        
    }
    /**
     * Teljes kepernyos modba (Full Screen Exclusive Mode) kapcsolas.
     * Ha nem tamogatott, akkor sima ablak fejlec es keret nelkul.
     */
    public void teljesKepernyosMod(java.awt.GraphicsDevice graphicsDevice) {
        
        int szelesseg = 0;
        int magassag = 0;
        // Nincs ablak fejlec, keret.
        setUndecorated(true);
        // Mi magunk fogunk rajzolni.
        setIgnoreRepaint(true);
        // Nincs atmeretezes
        setResizable(false);
        // At tudunk kapcsolni fullscreenbe?
        boolean fullScreenTamogatott = graphicsDevice.isFullScreenSupported();
        // Ha tudunk, akkor Full-Screen exkluziv modba valtunk
        if(fullScreenTamogatott) {
            graphicsDevice.setFullScreenWindow(this);
            // az aktualis kepernyo jellemzok (szelesseg, magassag, szinmelyseg,
            // frissitesi frekvencia) becsomagolt elkerese
            java.awt.DisplayMode displayMode
                    = graphicsDevice.getDisplayMode();
            // es kiiratasa
            szelesseg = displayMode.getWidth();
            magassag = displayMode.getHeight();
            int szinMelyseg = displayMode.getBitDepth();
            int frissitesiFrekvencia = displayMode.getRefreshRate();
            System.out.println(szelesseg
                    + "x"  + magassag
                    + ", " + szinMelyseg
                    + ", " + frissitesiFrekvencia);
            // A lehetseges kepernyo beallitasok elkerese
            java.awt.DisplayMode[] displayModes
                    = graphicsDevice.getDisplayModes();
            // Megnezzuk, hogy tamogatja-e az 1024x768-at, mert a
            // pelda jatekunkhoz ehhez a felbontashoz keszitettuk a kepeket
            boolean dm1024x768 = false;
            for(int i=0; i<displayModes.length; ++i) {
                if(displayModes[i].getWidth() == 1024
                        && displayModes[i].getHeight() == 768
                        && displayModes[i].getBitDepth() == szinMelyseg
                        && displayModes[i].getRefreshRate() 
                        == frissitesiFrekvencia) {
                    graphicsDevice.setDisplayMode(displayModes[i]);
                    dm1024x768 = true;
                    break;
                }
                
            }
            
            if(!dm1024x768)
                System.out.println("Nem megy az 1024x768, de a pelda kepmeretei ehhez a felbontashoz vannak allitva.");
            
        } else {
            setSize(szelesseg, magassag);
            validate();
            setVisible(true);
        }
        
        createBufferStrategy(2);
        
        bufferStrategy = getBufferStrategy();
        
    }
    /**
     * Atveszi a jatek inditasahoz szukseges parametereket, majd
     * elinditja a jatek vilaganak mukodeset.
     *
     * @param args a labirintus tervet tartalmazo allomany neve az elso 
     * parancssor-argumentum.
     */
    public static void main(String[] args) {
        
        if(args.length != 1) {
            
            System.out.println("Inditas: java LabirintusJatek labirintus.txt");
            System.exit(-1);
        }
        
        try {
            
            new LabirintusJatek(args[0]);
            
        } catch(RosszLabirintusKivetel rosszLabirintusKivetel) {
            
            System.out.println(rosszLabirintusKivetel);
            
        }
    }    
}
