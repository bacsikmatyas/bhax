<chapter xmlns="http://docbook.org/ns/docbook" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Lauda!</title>
        <keywordset>
            <keyword/>
        </keywordset>

    </info>

    <section>
        <title>Port scan</title>
        <para>
            A példánk egy egyszerű port scannelő program, ami a gépünk első 1024 portján végigmegy és megnézi, hogy mely portokat figyeli.
        </para>
        <para>
            Ezt a következő képpen teszi: 
        </para>
        <para>
            Megy egy for ciklus végig az 1024 porton. A cikluson belül elhelyezésre került egy try catch, melynek try blokkjában az adott portra megpróbálunk csatlakozni.
            Amennyiben ez sikerül, kiírjuk, hogy ezt a portot figyeli, majd megszakítjuk a kapcsolatot.
            Ha viszont nem sikerül, akkor errort dob, amit a catch-ünk elkap. Ilyenkor értelemszerűen kiírjuk, hogy ezt a portot nem figyeli.
        </para>

        <programlisting language="java"><![CDATA[public class KapuSzkenner {
    
            public static void main(String[] args) {
                
                for(int i=0; i<1024; ++i)
                    
                    try {
                        
                        java.net.Socket socket = new java.net.Socket(args[0], i);
                        
                        System.out.println(i + " figyeli");
                        
                        socket.close();
                        
                    } catch (Exception e) {
                        
                        System.out.println(i + " nem figyeli");
                        
                    }
            }
            
        }]]></programlisting>

    </section>

    <section>
        <title>AOP</title>
        <para>
            Feladatunk az AOP, azaz az Aspektus Orientált Programozás bemutatása rövid példákon keresztü.
            Ehhez egy egyszerű java programot fogunk használni, melybe új szálakat "szövünk" majd be az AspectJ segítségével.
            Ezeket a "szövéseket" advice-oknak, azaz "tanács"-oknak, a pontot ahova pedig beszúrjuk őket pointcut-oknak, azaz "vágási pontoknak" nevezzük.
        </para>
        <para>
            A .java és .aj állományok tartalma a 6. prog2-es ppt-ről származik.
        </para>
        <para>
            Vegyünk először is a HellóVilág.java fileunkat. Tartalmaz egy helló() metódust, ami kiírja, hogy "HelloVIlag> Hello!".
            Ezt a main.ben meg is hívjuk, és ennyi az egész.
        </para>
        <programlisting language="java"><![CDATA[
public class HellóVilág {

    public void helló(){
        System.out.println("HelloVilag> Hello!");
    }

    public static void main(String[] args) {

        //System.out.println("alma");

        new HellóVilág.helló();

        //System.out.println("körte");
    }
}]]></programlisting>
        <para>
            Ezen kis egyszerű program segítségével fogjuk demonstrálni, hogy akár egy összetetted programot is, hogyan láthatunk el akár nyomkövető üzenetekkel,
            adhatunk hozzá utólag új funkcionalitást, például naplózást. A mi kis programunkat itt az előbbivel szereljük fel.
        </para>
        <para>
            Hogy ezt hogyan is tesszük pontosan, ahhoz vessünk egy pillantást az ElőtteUtána.java filera.
        </para>
        <para>
            Először is létrehozunk egyúj aspektust, az aspect kulcsszó segítségével, hasonlóan a Javahoz, ennek neve megegyezik a file nevével.
        </para>
        <para>
            Definiálunk egy új vágási pontot fgvHívás néven és megadjuk neki, hogy amint a neve is mutatja, a vágási pont a függvényünk hívásánál helyezkedjen el.
        </para>
        <para>
            Ezután a before és after függvények(?) segítségével megadhatjuk, hogy a függvény hívása előtt és után mit csináljon.
            Esetünkben előtte kisírjuk, hogy alma, utána pedig hogy körte.
        </para>
        <programlisting><![CDATA[
public aspect ElőtteUtána {
    public pointcut fgvHivás() : call (public void HellóVilág.helló());

    before(): fgvHivás() {
        System.out.println("ElotteUtana> alma");
    }

    after(): fgvHivás() {
        System.out.println("ElotteUtana> körte");
    }
}]]></programlisting>

    <para>
        Fordításához és futtatásához a következő parancsokat használjuk:
    </para>
    <screen>
ajc HellóVilág.java ElőtteUtána.aj
java HellóVilág
    </screen>
    <para>
        Az ajc fordítóprogram végzi el a szövést az ElőtteUtána fileunk alapján.
    </para>

    <para>
        Fordítás után a következő kimenetet kapjuk, ha minden jól ment:
    </para>
    <screen>
ElotteUtana> alma
HelloVilag> Hello!
ElotteUtana> körte
    </screen>

    </section>

    <section>
        <title>Junit teszt</title>
        <para>
            Érdemes lenne azzal kezdeni, mi is a JUnit? Miért is jó nekünk a használata? A JUnit nem más, mint egy keretrendszer amit egységteszteléshez szokás használni Java nyelv mellé. Utóbbi fogalom kis magyarázatra szorul: akkor beszélünk egységtesztelésről, amikor egy adott kóddal együtt az adott kódot tesztelő osztállyal együtt kerül fejlesztésre.
         </para>
         <para>
             Lássunk kódot! (forrása: <link xlink:href="https://sourceforge.net/projects/udprog/"> UDPROG repó</link> )
         </para>
        
         <programlisting language="Java"><![CDATA[
 
         public class BinfaTest {
         LZWBinFa binfa = new LZWBinFa();
 
         @org.junit.Test
             
         public void tesBitFeldolg() {
             for (char c : "01111001001001000111".toCharArray()) 
             {
                 binfa.egyBitFeldolg(c);
             }
             
          ]]></programlisting>
           <para>
             AZ első két sorban semmiféle újdonságot nem találunk. Azonban a harmadik sorban már láthatunk számunkra eddig ismeretlen dolgokat, ezért a kód elemzését kezdjük azzal! A @-cal kezdődő sor jelöli annak a metódusnak a kezdetét, amit a JUnit tesztfuttatójának futtatnia kell. Viszont fontos, ha több ilyen metódus is van, azok végrehajtásának sorrendje nem lesz egyértelmű, ezért úgy érdemes kialakítani ezeket a teszteket, hogy függetlenek legyenek.</para>
             <para> Egy teszteset <function>@org.junit.Test</function> annotációval ellátott metódussal kezdődik, törzsében pedig a tesztelendő metódus kerül meghívásra. Az itt kapott eredményt szükséges összhasonlítanunk az általunk várttal.
         </para>
         <para>
             Léteznek egyébként másfajta annotációk is, ezekről is <link xlink:href="https://gyires.inf.unideb.hu/GyBITT/21/ch03s02.html"> itt</link> olvashatunk.
         </para>
         <para>
             Nem olyan rég óta van lehetőségünk parametrizált teszteket is végezni, azonban ez már egy haladóbb technika, mi nem foglalkozunk vele.
         </para>
 
          <para>
              A törzsben található a <function>tesBitFeldolg</function> függvényünk, ami a tesztesetünk lesz. Az <function>egyBitFeldolg</function> függvénnyel karakterenként dolgozzuk fel a megadott tömböt.
          </para>
 <programlisting language="Java"><![CDATA[
          org.junit.Assert.assertEquals(4, binfa.getMelyseg(), 0.0);
             org.junit.Assert.assertEquals(2.75, binfa.getAtlag(), 0.001);
             org.junit.Assert.assertEquals(0.957427, binfa.getSzoras(), 0.0001);
         }
     }
     ]]></programlisting>
 
     <para>
         Az <function>assertEquals</function> függvénnyel pedig megvizsgáljuk, mennyi is az eltérés - vagyis inkább, hogy megegyezik-e a két érték - a várható mélység, átlag és szórás valamint ezek tényleges értékei között. A függvény rendre, mindhárom esetben három paraméterrel rendelkezik. Az első az az érték, amit 'kapnunk kellene', a második a saját programunk által kapott érték, míg a harmadik a 'hibahatár' lényegében, vagyis hogy mennyi lehet a maximális eltérés az előbb említett két érték között.
     </para>
     <para>
         A teszteket Eclipse segítségével próbáltam tesztelni, de amikor a képeket akartam készíteni valamiért nem indult. Update inc.
     </para>
 
 
    </section>

    <section>
        <title>_</title>
        <para>
            _
        </para>
    </section>

</chapter> 
