<chapter xmlns="http://docbook.org/ns/docbook" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gödel!</title>
        <keywordset>
            <keyword/>
        </keywordset>

    </info>
    <section>
        <title>Gengszterek</title>

        <para>
            Feladatunk a Robotautó Világbajnokságban található egyik lambda kifejezés elemzése/értelmezése.
        </para>

        <para>
            Maga a kifejezés az std::sort függvény 3. paramétereként szerepel, azaz ő lesz az aki eldönti, hogy hogyan is történik a rendezés.
            Nézzük hogyan is épül fel.
        </para>

        <programlisting language="c++"><![CDATA[
std:sort(gangsters.begin(), gangsters.end(), 
[this, cop] (unsigned x, unsigned y) 
{
    return dst(cop, x) < dst(cop, y) 
} );
                        ]]></programlisting>
        <para>
            <emphasis>
                <emphasis role="bold">"Capture clause":</emphasis>
            </emphasis>
        </para>
        <para>
            A kifejezés elején, "szögletes" zárójelben lehet megadni külső változókat (kapcsoló alapján érték vagy referencia szerint). 
            Jelen esetben itt a this kulcsszó szerepel valamint a cop objektum. A 'this' használatával a lambda kifejezésünk az őt magában foglaló osztályban mindent elér.
        </para>
        <para>
            <emphasis>
                <emphasis role="bold">Paraméter lista:</emphasis>
            </emphasis>
        </para>
        <para>
            Ez egy magától értetődő szakasz, adhatunk meg paramétereket a capture clause-ból kapott értékek mellé.
        </para>
        <para>
            <emphasis>
                <emphasis role="bold">A lambda kifejezés teste:</emphasis>
            </emphasis>
        </para>
        <para>
            Lehetnek még különböző plusz részei, de a példánkban már csak ez van hátra. Itt adhatjuk meg hogy konkrétan mit is csináljon.
            Használhatjuk a this kulcsszó miatt az osztály összes attribútumát, valamint a paramétereket erre a célra.
            A hagyományos módon megadott függvényekhez hasonlóan, az eredményünket egy return segítségével adjuk vissza.
            Véleményem szerint a feladat lényege a lamba kifejezések ismertetése volt, nem pedig a konkrét kódé, úgyhogy azt hogy pontosan mi is történik itt, azt nem fejtem ki.
        </para>
        <para>
            <emphasis>
                <emphasis role="bold">"Extrák":</emphasis>
            </emphasis>
        </para>
        <para>
            Csak felsorolás szintjén megemlítenék még pár 'fícsört', ami jelen esetben elmaradt, de kiegészíthetjük vele a lambdánkat.
            Megadhatjuk, hogy az "elfogott" (captured) változk értékét módosíthatja-e, megadhatjuk, hogy milyen kivételeket fog dobni, valamint a visszatérési érték típusát is beállíthatjuk.
            Ezek mindegyike elhagyható.
        </para>



    </section>

    <section>
        <title>C++11 Custom Allocator</title>

        <para>
	        Feladatunk a C++ Custom Allocator példa feldolgozása a <link xlink:href="https://prezi.com/jvvbytkwgsxj/high-level-programming-languages-2-c11-allocators/">prezentációból.</link>
        </para>

        <para>
            C++-ban a különböző adattároló konténerek számára az allokátorok biztosítanak helyet a memóriában, erre alapértelmezésben az std könyvtár allocator függvényét használják.
            Természetesen definiálhatunk sajátokat is, erre mutat példát a feladat.
        </para>
        <para>
            A kivitelezéshez definiáljuk a CustomAlloc nevű struktúrát, valamint a T typename templatet(Ennek segítségével fog működni a példánk különböző típusok esetén is).
            A struct elején definiálunk pár aliast, a könnyebb átláthatóság érdekében, valamint megadjuk a konstruktort, másoló konstruktor és destruktort.
        </para>

        <programlisting language="c++"><![CDATA[
template <typename T>
struct CustomAlloc {
    using size_type = size_t;
    using value_type = T;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;
    using difference_type = ptrdiff_t;

    CustomAlloc() {}
    CustomAlloc(const CustomAlloc &) {}
    ~CustomAlloc() {}
                                    ]]></programlisting>

        <para>
            Majd következik maga az allocate függvény. Pointert (azaz T*-ot) ad vissza, ami a lefoglalt helyre fog mutatni. paraméternek egy méret típusú változót kap, ami az objektumok darabszámát mondja meg, melyeknek a memóriát foglaljuk.
        </para>
        <para>
            Létrehozunk egy s int típusú változót, amiben a p char pointer értékét beállító függvény státuszát fogjuk tárolni. a p-ben pedig az abi könyvtár __cxa_demangle függvényének segítségével megkapjuk, hogy milyen típusú változónak is fglalunk helyet jelenleg.
            Ezekre a nyomkövető üzenetek miatt van szükség jelen esetben.
        </para>
        <para>
            Az említett nyomkövető üzenetek után a p-t fel is szabadítjuk, nem lesz rá több szükség.
        </para>
        <para>
            Majd végül a lényeg, a konkrét foglalás. A new kulcssszóval foglalunk n darab adott memóriaméretű tömböt(a kódban itt T szerepel, de a template miatt ide mindig az adott típus fog bekerülni), amely char pointer lesz, de a reinterpret_cast függvény segítségével megadjuk a konkrét típusát és ezt a return segítségével vissza is adjuk.
        </para>

        <programlisting language="c++"><![CDATA[
pointer allocate (size_type n){
    int s;
    char* p = abi::__cxa_demangle( typeid(T).name(), 0, 0, &s);

    std::cout   << "Allocating: "
                << n << "objects of "
                << n*sizeof(T)
                << "bytes."
                << typeid(T).name() << "=" p
                << std::endl;
    free(p);

    return reinterpret_cast< T* > (new char [n*sizeof (T)]);

}
        ]]></programlisting>
        <para>
            Végül vessünk egy pillantást az utolsó, deallocate függvényre. nincs visszatérési értéke, paraméternek a pointert és a méretét kéri.
        </para>
        <programlisting language="c++"><![CDATA[
    void deallocate(pointer p, size_type n) {
        delete[] reinterpret_cast<char *> (p);
    }
}
        ]]></programlisting>

        <para>
            CustomAlloc használata például egy int vector esetén:
        </para>
        <programlisting language="c++">
<![CDATA[ std::vector<int, CustomAlloc<int>> vektor; ]]>
        </programlisting>


    </section>

    <section>
        <title>STL map érték szerinti rendezése</title>

        <para>
	        A példaként említett metódus (sort_map) paraméterként egy std::map-et vár strin, int értékpárokkal és ennek a mapnek a rendezett változatát fogja visszaadni egy string int párokat tartalmazó vektor formájában.
        </para>
        <para>
            Ehhez először is létrehozza azt a vektort, amit vissza fogunk adni, orderd néven, majd végigmegy a kapott rank nevezetű map-en és tartalmát az ordered-be másolja.
        </para>
        <para>
            Ezután következik a sort metódus, ami felépítésében hasonlít a csokor első feladatához, első és második paramétere az ordered vektor eleje és vége, azaz az egészet fogjuk rendezni, a harmadik paraméterként megadott lambda kifejezés segítségével.
        </para>

        <para>
            A kifejezés capture része egy = jelet tartalmaz, ami beállítja, hogy a capture-ben kapott változókat érték szerint "kapja el". Mivel ezt semmi nem követi így nem tudom egyáltalán mi értelme van ennek.
            Paraméterként p1 és p2, 2 jobb-érték változót vár, melyeknek típusát az auto kulcsszó segítségével automatikusan detektáljuk. 
            A kifejezés testében teszteljük, hogy a p1 második tagja (p1.second) vagy a p2 második tagja-e(p2.second) a nagyobb és ennek a kifejezésnek az igazságértékét adja vissza.
            Tehát ennek segítségével rendezzük a vektort és vissza is adjuk.
        </para>

        <programlisting language="c++"><![CDATA[
std::vector<std::pair<std::string, int>> sort_map ( std::map <std::string, int> &rank )
    {
            std::vector<std::pair<std::string, int>> ordered;
    
            for ( auto & i : rank ) {
                    if ( i.second ) {
                            std::pair<std::string, int> p {i.first, i.second};
                            ordered.push_back ( p );
                    }
            }
    
            std::sort (
                    std::begin ( ordered ), std::end ( ordered ),
            [ = ] ( auto && p1, auto && p2 ) {
                    return p1.second > p2.second;
            }
            );
    
            return ordered;
    }
        ]]></programlisting>


    </section>


    <section>
        <title>Alternatív tabella rendezés</title>

        <para>
	        Feladatunk a következő példában a Comparable java interfész szerepének bemutatása.
        </para>
        <para>
            Az interfész egyetlen tagja a compareTo metódus. Implementálásával lehetőségünk van definiálni hogy az adott osztály két példánya között milyen viszony áll fent(kisebb, nagyobb, egyenlő).
        </para>

        <para>
            A Csapat osztály defniálásánál láthatjuk hogy az implements kulcsszóval implementáljuk a Comparable interfészt.
            Ezek mellet az osztálynak lesz egy String típusú nev tagja és egy double típusú ertek tagja, már itt is látható, hogy nem egyértelmű, ha esetleg egy Csapat típusú objektumokat tároló tárolót rendezünk, hogy mi alapján tegyük is ezt. Erre lesz tökéletes megoldás a compareTo metódus megadása.
            A konstruktorunk semmi extra.
        </para>
        <para>
            A compareTo metódus definiálásánál láthatjuk, hogy egy intértéket ad vissza és paraméterként egy Csapat objektumot vár.
            A visszaadott int 3 dolgot jelképezhet. Amennyiben negatív(itt: -1), a kapott csapat ertek változója(csapat.ertek) nagyobb mint az adott Csapat objektum ertek változója(this.ertek).
            Ha a kapott ertek kisebb mint az adott objektum ertek-e akkor pozitív egésszet ad vissza. Viszont ha ezek közül egyik sem, akkor 0-val tér vissza(azaz amennyiben a 2 ertek egyenlő).
        </para>

        <programlisting language="java"><![CDATA[
class Csapat implements Comparable<Csapat> {

    protected String nev;
    protected double ertek;
    
    public Csapat(String nev, double ertek) {
        this.nev = nev;
        this.ertek = ertek;
    }
    
    public int compareTo(Csapat csapat) {
        if (this.ertek < csapat.ertek) {
        return -1;
        } else if (this.ertek > csapat.ertek) {
        return 1;
        } else {
        return 0;
        }
    }
}
                ]]></programlisting>


    </section>

</chapter> 
